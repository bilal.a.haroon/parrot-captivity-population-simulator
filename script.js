const canvas = document.getElementById("canvas");

const ctx = canvas.getContext("2d");

var parrot_list = [[]];
// make a parrot
function drawParrot(x, y, color) {
  ctx.beginPath();
  ctx.fillStyle = color;
  ctx.arc(x, y, 5, 0, Math.PI * 2);
  ctx.fill();
}

// check if the colors of the last row are not the same
function colorsNotSame() {
  // grab the first color of the last row
  initialColor = parrot_list[parrot_list.length - 1][0];

  // itirate through all the other colors
  for (var i = 1; i < parrot_list[parrot_list.length - 1].length; i++) {
    // if we find a color that does not equal the first color return true

    console.log("index: " + i);
    console.log(
      "initial color: " +
        initialColor +
        ", Checking: " +
        parrot_list[parrot_list.length - 1][i]
    );

    if (parrot_list[parrot_list.length - 1][i] !== initialColor) {
      console.log("true");
      console.log("");
      return true;
    }
  }
}

// create a new row of parrots
function addGeneration(children_Num) {
  // The parents are the last row
  const parents = parrot_list[parrot_list.length - 1];

  // make a new row for the children
  children = [];

  for (var i = 1; i < children_Num + 1; i++) {
    // grab two random parents
    const mom = parents[Math.floor(Math.random() * parents.length)];
    const dad = parents[Math.floor(Math.random() * parents.length)];

    // if both are the same color make the children that color
    if (mom === "blue" && dad == "blue") {
      children.push("blue");
    } else if (mom === "red" && dad == "red") {
      children.push("red");
    }
    // else make the children a random color
    else {
      const randColor = Math.random();
      if (randColor > 0.5) {
        children.push("blue");
      } else {
        children.push("red");
      }
    }
  }

  // add the children row to the list
  parrot_list.push(children);
}

//draw the parrots
function render() {
  ctx.clearRect(0, 0, canvas.width, canvas.height);
  for (var i = 0; i < parrot_list.length; i++) {
    for (var j = 0; j < parrot_list[i].length; j++) {
      drawParrot(10 + 11 * j, 10 + 11 * i, parrot_list[i][j]);
    }
  }
  parrot_list = [[]];
}

// when the sim button is clicked
document.getElementById("sim").onclick = function() {
  const N = document.getElementById("N").value;
  console.clear();
  // add origional parrots
  for (var i = 0; i < N; i++) {
    parrot_list[0].push("blue");
    parrot_list[0].push("red");
  }

  while (colorsNotSame()) {
    addGeneration(N * 2);
  }

  // clear the canvas
  render();
};
